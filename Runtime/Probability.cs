using UnityEngine;

namespace Deaven.ProbabilitySets
{
    public class Probability<T> where T : ScriptableObject
    {
        public float Value;
        public T Data;

        public Probability(float value, T data)
        {
            Value = value;
            Data = data;
        }
    }
}