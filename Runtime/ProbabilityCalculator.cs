using System;
using Deaven.RuntimeSets;
using Sirenix.OdinInspector;
using UnityEngine;
using Random = System.Random;

namespace Deaven.ProbabilitySets
{
    public abstract class ProbabilityCalculator<TRuntimeSet, TData> : ScriptableObject
        where TRuntimeSet : RuntimeSet<TData>
        where TData : ScriptableObject
    {
        public TRuntimeSet set;

        [SerializeField] private ProbabilitySet<TRuntimeSet, TData> probabilitySet;

        public event Action ValueChanged;

        public ProbabilitySet<TRuntimeSet, TData> ProbabilitySet => probabilitySet;

        [Button]
        public void CalculateProbability()
        {
            probabilitySet = new ProbabilitySet<TRuntimeSet, TData>(set);
            ValueChanged?.Invoke();
        }

        /// <summary>
        /// Selects data from given probability set based on their probability
        /// </summary>
        /// <returns></returns>
        public TData Select()
        {
            Random random = new Random();
            float rand = (float) random.NextDouble();

            foreach (Probability<TData> hero in probabilitySet.Probabilities)
            {
                if (hero.Value > rand)
                    return hero.Data;
            }

            // Return most possible data (last data in set)
            return probabilitySet.Probabilities[probabilitySet.Probabilities.Count - 1].Data;
        }

        public float GetProbability(TData data)
        {
            float probabilityValue = 0;

            foreach (Probability<TData> probability in probabilitySet.Probabilities)
            {
                if (probability.Data == data)
                {
                    probabilityValue = probability.Value;
                    break;
                }
            }

            return probabilityValue;
        }
    }
}