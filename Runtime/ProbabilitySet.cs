using System;
using System.Collections.Generic;
using Deaven.RuntimeSets;
using Sirenix.OdinInspector;
using UnityEngine;
using Random = System.Random;

namespace Deaven.ProbabilitySets
{
    [Serializable]
    public class ProbabilitySet<TRuntimeSet, TData> 
        where TRuntimeSet : RuntimeSet<TData>
        where TData : ScriptableObject
    {
        public float Sum { get; private set; }

        private Random random = new Random();

        [ShowInInspector] public List<Probability<TData>> Probabilities { get; private set; }

        public ProbabilitySet(TRuntimeSet set)
        {
            Probabilities = new List<Probability<TData>>();

            CalculateSum(set);
            NormalizeProbabilities();
            SortProbabilities();
        }

        /// <summary>
        /// Creates a random value for each Item in given set and creates a sum 
        /// based on these values.
        /// These values are then used to determine the probability of each set item 
        /// </summary>
        /// <param name="set"></param>
        private void CalculateSum(TRuntimeSet set)
        {
            foreach (TData data in set.RuntimeValues)
            {
                int randomValue = random.Next(0, 100);
                
                Sum += randomValue;
                Probabilities.Add(new Probability<TData>(randomValue, data));
            }
        }

        /// <summary>
        /// Normalizes all Probabilities between 0 and 1
        /// </summary>
        private void NormalizeProbabilities()
        {
            foreach (Probability<TData> probability in Probabilities)
            {
                probability.Value /= Sum;
            }
        }

        /// <summary>
        /// Sorts Probabilities items in descending order based on their probability
        /// </summary>
        private void SortProbabilities()
        {
            Probabilities.Sort(delegate(Probability<TData> x, Probability<TData> y)
            {
                if (x.Value > y.Value)
                    return 1;

                return -1;
            });
        }
    }
}